package ru.t1.amsmirnov.taskmanager;

import ru.t1.amsmirnov.taskmanager.component.Bootstrap;

public final class Application {

    public static void main(String[] args){
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
